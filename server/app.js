var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var equipos = [ "Navegantes del Magallanes", "Leones del Caracas", "Tiburones de la Guaíra", "Cardenales de Lara", "Caribes de Oriente", "Tigres de Araugua", "Aguilas del Zulia", "Bravos de Margarita" ];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var preferidos = [];
app.get("/my", (req, res, next) => res.json(preferidos));
app.post("/my", (req, res, next) => {
  console.log(req.body);
  preferidos.push(req.body.nuevo);
  res.json(preferidos);
});
app.get("/api/translation", (req, res, next) => res.json([
  {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));

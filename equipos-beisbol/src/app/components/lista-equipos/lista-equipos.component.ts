import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { EquipoVenezolano } from './../../models/equipo-venezolano.model';
import { EquiposApiClient } from './../../models/equipos-api-client.model';
import { Store } from '@ngrx/store';
import {AppState} from './../../app.module';

@Component({
  selector: 'app-lista-equipos',
  templateUrl: './lista-equipos.component.html',
  styleUrls: ['./lista-equipos.component.css'],
  providers: [ EquiposApiClient ]
})
export class ListaEequiposComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<EquipoVenezolano>;
  updates:string[];

  constructor(
      private EquiposApiClient:EquiposApiClient,
      private store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit() {
    this.store.select(state => state.equipos.favorito)
      .subscribe(data => {
        const f = data;
        if (f != null) {
          this.updates.push('Se eligió: ' + f.nombre);
        }
      });
  }

  agregado(e:EquipoVenezolano) {
    this.EquiposApiClient.add(e);
    this.onItemAdded.emit(e);
  }

  elegido(e:EquipoVenezolano) {
    this.EquiposApiClient.elegir(e);
  }
}

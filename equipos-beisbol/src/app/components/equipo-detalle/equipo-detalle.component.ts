import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { EquiposApiClient } from './../../models/equipos-api-client.model';
import { EquipoVenezolano } from './../../models/equipo-venezolano.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-equipo-detalle',
  templateUrl: './equipo-detalle.component.html',
  styleUrls: ['./equipo-detalle.component.css'],
  providers: [EquiposApiClient]
})
export class EquipoDetalleComponent implements OnInit {
  equipo: EquipoVenezolano;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private EquiposApiClient: EquiposApiClient) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.equipo = this.EquiposApiClient.getById(id);
  }

}

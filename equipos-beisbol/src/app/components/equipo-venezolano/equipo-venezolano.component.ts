import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { EquipoVenezolano } from './../../models/equipo-venezolano.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/equipos-venezolanos-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-equipo-venezolano',
  templateUrl: './equipo-venezolano.component.html',
  styleUrls: ['./equipo-venezolano.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class EquipoVenezolanoComponent implements OnInit {
  @Input() equipo: EquipoVenezolano;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<EquipoVenezolano>;

  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.onClicked.emit(this.equipo);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.equipo));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.equipo));
    return false;
  }
}

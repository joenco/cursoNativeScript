import {
  reducerEquiposVenezolanos,
  EquiposVenezolanosState,
  intializeEquiposVenezolanosState,
  InitMyDataAction,
  NuevoEquipoAction
} from './equipos-venezolanos-state.model';
import { EquipoVenezolano } from './equipo-venezolano.model';

describe('reducerEquiposVenezolanos', () => {
  it('should reduce init data', () => {
    const prevState: EquiposVenezolanosState = intializeEquiposVenezolanosState();
    const action: InitMyDataAction = new InitMyDataAction(['equipo 1', 'equipo 2']);
    const newState: EquiposVenezolanosState = reducerEquiposVenezolanos(prevState, action);
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('equipo 1');
  });

  it('should reduce new item added', () => {
    const prevState: EquiposVenezolanosState = intializeEquiposVenezolanosState();
    const action: NuevoEquipoAction = new NuevoEquipoAction(new EquipoVenezolano('magallanes', 'url'));
    const newState: EquiposVenezolanosState = reducerEquiposVenezolanos(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('magallanes');
  });
});

import {Injectable, Inject, forwardRef} from '@angular/core';
import { EquipoVenezolano } from './equipo-venezolano.model';
import { Store } from '@ngrx/store';
import {
    NuevoEquipoAction,
    ElegidoFavoritoAction
  } from './equipos-venezolanos-state.model';
import {AppState, APP_CONFIG, AppConfig, MyDatabase, db} from './../app.module';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';

@Injectable()
export class EquiposApiClient {
  equipos: EquipoVenezolano[] = [];

  constructor(
    private store: Store<AppState>,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
  ) {
    this.store
      .select(state => state.equipos)
      .subscribe((data) => {
        console.log('equipos sub store');
        console.log(data);
        this.equipos = data.items;
      });
    this.store
      .subscribe((data) => {
        console.log('all store');
        console.log(data);
      });
  }

  add(e: EquipoVenezolano) {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: e.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoEquipoAction(e));
        const myDb = db;
        myDb.equipos.add(e);
        console.log('todos los equipos de la db!');
        myDb.equipos.toArray().then(equipos => console.log(equipos))
      }
    });
  }

  getById(id: String): EquipoVenezolano {
    return this.equipos.filter(function(e) { return e.id.toString() === id; })[0];
  }

  getAll(): EquipoVenezolano[] {
    return this.equipos;
  }

  elegir(e: EquipoVenezolano) {
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }
}

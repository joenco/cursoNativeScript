import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EquipoVenezolano } from './equipo-venezolano.model';
import { EquiposApiClient } from './equipos-api-client.model';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface EquiposVenezolanosState {
    items: EquipoVenezolano[];
    loading: boolean;
    favorito: EquipoVenezolano;
}

export function intializeEquiposVenezolanosState() {
  return {
    items: [],
    loading: false,
    favorito: null
  };
}

// ACCIONES
export enum EquiposVenezolanosActionTypes {
  NUEVO_EQUIPO = '[Equipos Venezolanos] Nuevo',
  ELEGIDO_FAVORITO = '[Equipos Venezolanos] Favorito',
  VOTE_UP = '[Equipos Venezolanos] Vote Up',
  VOTE_DOWN = '[Equipos Venezolanos] Vote Down',
  INIT_MY_DATA = '[Equipos Venezolanos] Init My Data'
}

export class NuevoEquipoAction implements Action {
  type = EquiposVenezolanosActionTypes.NUEVO_EQUIPO;
  constructor(public equipo: EquipoVenezolano) {}
}

export class ElegidoFavoritoAction implements Action {
  type = EquiposVenezolanosActionTypes.ELEGIDO_FAVORITO;
  constructor(public equipo: EquipoVenezolano) {}
}

export class VoteUpAction implements Action {
  type = EquiposVenezolanosActionTypes.VOTE_UP;
  constructor(public equipo: EquipoVenezolano) {}
}

export class VoteDownAction implements Action {
  type = EquiposVenezolanosActionTypes.VOTE_DOWN;
  constructor(public equipo: EquipoVenezolano) {}
}

export class InitMyDataAction implements Action {
  type = EquiposVenezolanosActionTypes.INIT_MY_DATA;
  constructor(public equipos: string[]) {}
}

export type EquiposVenezolanosActions = NuevoEquipoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | InitMyDataAction;

// REDUCERS
export function reducerEquiposVenezolanos(
  state: EquiposVenezolanosState,
  action: EquiposVenezolanosActions
): EquiposVenezolanosState {
  switch (action.type) {
    case EquiposVenezolanosActionTypes.INIT_MY_DATA: {
      const equipos: string[] = (action as InitMyDataAction).equipos;
      return {
          ...state,
          items: equipos.map((e) => new EquipoVenezolano(e, ''))
        };
    }
    case EquiposVenezolanosActionTypes.NUEVO_EQUIPO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoEquipoAction).equipo ]
        };
    }
    case EquiposVenezolanosActionTypes.ELEGIDO_FAVORITO: {
        state.items.forEach(x => x.setSelected(false));
        const fav: EquipoVenezolano = (action as ElegidoFavoritoAction).equipo;
        fav.setSelected(true);
        return {
          ...state,
          favorito: fav
        };
    }
    case EquiposVenezolanosActionTypes.VOTE_UP: {
        const e: EquipoVenezolano = (action as VoteUpAction).equipo;
        e.voteUp();
        return { ...state };
    }
    case EquiposVenezolanosActionTypes.VOTE_DOWN: {
        const e: EquipoVenezolano = (action as VoteDownAction).equipo;
        e.voteDown();
        return { ...state };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class EquiposVenezolanosEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(EquiposVenezolanosActionTypes.NUEVO_EQUIPO),
    map((action: NuevoEquipoAction) => new ElegidoFavoritoAction(action.equipo))
  );

  constructor(private actions$: Actions) {}
}

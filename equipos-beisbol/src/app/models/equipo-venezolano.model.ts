import {v4 as uuid} from 'uuid';

export class EquipoVenezolano {
  selected: boolean;
  servicios: string[];
  id = uuid();
  public votes = 0;
  constructor(public nombre: string, public imagenUrl: string) {
       this.servicios = ['Gorras', 'Camisetas', 'Chaquetas'];
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
